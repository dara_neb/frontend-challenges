import * as React from "react";

const NotificationCard = ({
  image = "",
  name = "",
  actionMessaging = "",
  passingTime = "",
}) => {
  return (
    <div>
      <img src={image} alt="User profile" />
      <p>{name}</p>
      <p>{actionMessaging}</p>
      <p>{passingTime}</p>
    </div>
  );
};

export { NotificationCard };
