import "./App.css";
import { NotificationCard } from "./NotificationCard";

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <div className="App-body">
          <div className="Container-style">
            <div className="Container-style">
              <p className="Header-title-text">{"Notifications"}</p>
              <p className="Header-title-text-badge">{"3"}</p>
            </div>{" "}
            <p className="Header-title-right-text">{"Mark all as read"}</p>
          </div>
          <NotificationCard />
        </div>
      </header>
    </div>
  );
}

export default App;
