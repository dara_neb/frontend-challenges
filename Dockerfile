FROM node:12-alpine as build

WORKDIR /src

COPY . /src/

RUN yarn install
RUN yarn global add react-scripts@3.0.1
RUN yarn run build

FROM nginx:1.16.0-alpine
COPY --from=build /src/build /usr/share/nginx/html
RUN rm /etc/nginx/conf.d/default.conf
COPY nginx/nginx.conf /etc/nginx/conf.d

EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]